<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: compte.php
Fonction: Formulaires de connection à un compte existant, et de création d'un nouveau compte. Affichage des messages d'erreur éventuels. Si l'utilisateur est connecté, redirection vers sa page d'accueil personnelle.
=========== -->


<?php
session_start();
if (isset($_SESSION['login'])) {
	header("Location:accueil.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Compte</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li class="selected">
						<a href="compte.php">Compte</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="about">
					<h2>Compte</h2>
					<img src="images/compteimg.jpg" alt="" class="center">

					<?php

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}


						if ( (!isset($_POST['connexion'])) && (!isset($_POST['inscription'])) ) {

							echo"<h4><span>Se connecter</span></h4></br>";
							echo"<div id=\"contenu\"><form action=\"compte.php\" method=\"post\">";
							echo"<fieldset><p>Pseudo : <input type=\"text\" name=\"login\" maxlength=\"16\" required=\"required\" placeholder=\"...\"/>";
							echo" Mot de Passe : <input type=\"password\" name=\"mdp\" required=\"required\" placeholder=\"...\"/> ";
							echo"<input type=\"submit\" name=\"connexion\" value=\"Connexion\"></fieldset></form></p>";
								
							if ((isset($_GET['error'])) && ($_GET['error'] == 1)) {
								echo("<p>Identifiant ou mot de passe erroné. Votre compte peut être désactivé. Contactez un gestionnaire.</p>");
							}
								
							echo"</div>
							</br>
							</br>
							<h4><span>Créer un compte</span></h4>";

							echo"</br>
							<div id=\"contenu\">

							<form action=\"compte.php\" method=\"post\">
							<fieldset>
							<p>Pseudo : <input type=\"text\" name=\"pseudo\" maxlength=\"16\" required=\"required\" placeholder=\"...\"/> </br></br>
							Mot de Passe : <input type=\"password\" name=\"mdp1\" placeholder=\"...\" required=\"required\"/>
							- Confirmez mot de passe: <input type=\"password\" name=\"mdp2\" placeholder=\"...\" required=\"required\"/></br></br>
							Nom: <input type=\"text\" name=\"nom\" maxlength=\"16\" required=\"required\" placeholder=\"...\" pattern=\"[A-Za-zÀ-ÿ '-]+\"/></br></br>
							Prénom: <input type=\"text\" name=\"prenom\" maxlength=\"16\" required=\"required\" placeholder=\"...\" pattern=\"[A-Za-zÀ-ÿ '-]+\"/></br></br>
							Adresse Mail: <input type=\"email\" name=\"mail\" maxlength=\"64\" required=\"required\" placeholder=\"...\"/></br></br>
							</p>
							<input type=\"submit\" name =\"inscription\" value=\"Valider\">
							</fieldset>
							</form>";
							if ((isset($_GET['error'])) && ($_GET['error'] == 2)) {
								echo("<p>Erreur dans la confirmation de votre mot de passe.</p>");
							}
							echo"</div>";

						} else if (isset($_POST['connexion'])) {

							$id=htmlspecialchars(addslashes($_POST['login']));
							$mdp=htmlspecialchars(addslashes($_POST['mdp']));

							//vérifie l'existence d'un compte et d'un profil valide qui correspond aux identifiants de connexion renseignés.
							$sql="SELECT * FROM t_compte JOIN t_profil USING (compte_pseudo) WHERE compte_pseudo='" . $id . "' AND compte_mdp = MD5('" . $mdp . "') AND pro_valide = 'A';";
						 	if (!$result = $mysqli->query($sql)) {
								echo "Error: Problème d'accès à la base \n";
								exit();
							} else {
								$ligne = $result->fetch_object();
								if($result->num_rows == 1) {

									$_SESSION['login']=$id;
									$_SESSION['statut']=$ligne->pro_statut;

									header("Location:accueil.php");

								} else {
									header("Location:compte.php?error=1");
									exit();
								}
							}


						} else if (isset($_POST['inscription'])) {

							$mdp1=htmlspecialchars(addslashes($_POST['mdp1']));
	                    	$mdp2=htmlspecialchars(addslashes($_POST['mdp2']));

	                    	$id=htmlspecialchars(addslashes($_POST['pseudo']));


			/*2*/			if (strcmp($mdp1, $mdp2) == 0) {
								$mdp=$mdp1;
								$erreurmdp = 0;
							} else {
								$erreurmdp = 1;
							}

							$nom=htmlspecialchars(addslashes($_POST['nom']));
							$prenom=htmlspecialchars(addslashes($_POST['prenom']));
							$mail=htmlspecialchars(addslashes($_POST['mail']));


			/*3*/			if ($erreurmdp == 1) {

								header("Location: compte.php?error=2");
								exit();

							} else {

								echo ("<h4><span> Bonjour, ");
								echo ($id);
								echo ("</span></h4>");

								//Requête d'insertion d'une ligne dans la table des comptes
			/*4.1.1*/			$sql="INSERT INTO t_compte VALUES('" .$id.
								"', MD5('" .$mdp."'));";

			/*4.1.2*/			if (!$result1 = $mysqli->query($sql)) {
									echo "Error: La requête t_compte a échoué \n";
									echo "Query: " . $sql . "\n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {
									echo "<p>Compte créé.</p>" . "\n";
				/*4.1.3*/	
									//Requête d'insertion d'une ligne dans la table des profils
									$sql="INSERT INTO t_profil VALUES('" .$id.
									"', '" .$nom."', '".$prenom."', '".$mail."', 'D', 'M', CURRENT_TIMESTAMP);";
									if (!$result2 = $mysqli->query($sql)) {
									 	// La requête a echoué
										echo "Error: La requête t_profil a échoué \n";
										echo "Query: " . $sql . "\n";
										echo "Errno: " . $mysqli->errno . "\n";
										echo "Error: " . $mysqli->error . "\n";
										//Requête de suppression d'une ligne dans la table des comptes si l'insertion du profil a échoué.
										$sql="DELETE FROM t_compte WHERE compte_pseudo= '".$id."';";
										if (!$result3 = $mysqli->query($sql)) {
										 	echo "Error: impossible de supprimer dans t_compte.\n";
											echo "Query: " . $sql . "\n";
											echo "Errno: " . $mysqli->errno . "\n";
											echo "Error: " . $mysqli->error . "\n";
											exit();
										} else {
											echo "<br />";
											echo "Compte supprimé" . "\n";
										}
									 	exit();
									} else {
										echo "<p>Profil créé. Contactez un gestionnaire pour l'activation de votre compte! </p>" . "\n";
									}
								}
							}
						}

						$mysqli->close();

					?>

					
				</div>
			</div>
		</div>
	</div>
</body>
</html>