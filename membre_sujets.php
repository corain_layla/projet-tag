<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: membre_sujets.php
Fonction: Accessible uniquement par un membre connecté. Affichage, création, modification, suppression des sujets personnels de l'utilisateur connecté.
=========== -->

<?php
session_start();
if((!isset($_SESSION['login'])) || (($_SESSION['statut']) != 'M')) {
	header("Location:index.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Espace Membre</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="compte.php">Compte</a>
					</li>
					<li>
						<a href="membre_actus.php">Mes Actus</a>
					</li>
					<li class="selected">
						<a href="membre_sujets.php">Sujets</a>
					</li>
					<li>
						<a href="gestion_tags.php">Tags</a>
					</li>
					<li>
						<a href="deconnection.php">Deconnection</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="about">
					<h2>Gestion des sujets</h2>

					<?php

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}

						if ( (!isset($_POST['creer'])) && (!isset($_POST['modifier'])) && (!isset($_POST['supprimer'])) ) {

						echo "<h4><span>Créer un sujet</span></h4>";

						echo"<div id=\"contenu\">";
						echo"<form action=\"membre_sujets.php\" method=\"post\"><p>";
						echo"Intitulé : <input name=\"intitule\" type=\"text\" maxlength=\"16\" required=\"required\" placeholder=\"...\">";
						echo"</p><input type=\"submit\" name =\"creer\" value=\"Créer\"></form></br>";


						echo "<h4><span>Mes sujets</span></h4>";

						//Affichage de tous les sujets créés par l'utilisateur connecté.
						$sql="SELECT * FROM t_sujet WHERE compte_pseudo = '".$_SESSION['login']."';";
						if (!$result = $mysqli->query($sql)) {
							echo "Error: La requête a echoué \n";
							echo "Errno: " . $mysqli->errno . "\n";
							echo "Error: " . $mysqli->error . "\n";
							exit();
						} else {
							echo("<form action=\"membre_sujets.php\" method=\"post\">");
							echo "<p>Sélectionnez un sujet</br><select name=\"sujet\">";
							while ($suj = $result->fetch_assoc()) {
								echo ("<option value=\"".$suj['suj_id']."\">" . $suj['suj_intitule'] . "</option>");
							}

							echo"</select>";
							echo"  <input type=\"submit\" name =\"supprimer\" value=\"Supprimer sujet\"> Supprimer un sujet supprimera également tous ses tags associés.</br></br>";
							echo"Ou modifiez le sujet : <input name=\"newintitule\" type=\"text\" maxlength=\"16\" placeholder=\"Nouvel intitulé\">";
							echo" <input type=\"submit\" name =\"modifier\" value=\"Modifier\"></form></p>";
							
						}

						if (isset($_GET['msg'])) {
							if ($_GET['msg'] == 1) {
								echo("<p>Sujet ajouté.</p>");
							} else if ($_GET['msg'] == 2) {
								echo("<p>Sujet modifié.</p>");
							} else if ($_GET['msg'] == 3) {
								echo("<p>Sujet supprimé.</p>");
							} else if ($_GET['msg'] == 4) {
								echo("<p>Veuillez insérer un titre valide.</p>");
							}
						}

						echo"</div>";


						} else if (isset($_POST['creer'])) {

							$int=htmlspecialchars(addslashes($_POST['intitule']));

							//Insertion d'un nouveau sujet dans la base de donnée.
							$sql1="INSERT INTO t_sujet values ('".$_SESSION['login']."', null, '".$int."', CURRENT_TIMESTAMP);";
							if (!$result1 = $mysqli->query($sql1)) {
								echo "Error: La requête a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								exit();
							} else {
								header('Location: membre_sujets.php?msg=1');
								exit();
							}


						} else if ((isset($_POST['modifier'])) && (isset($_POST['newintitule'])) ) {

							if ($_POST['newintitule'] != '') {

								$int=htmlspecialchars(addslashes($_POST['newintitule']));

								//Modification d'un sujet dont l'identifiant est connu.
								$sql2="UPDATE t_sujet SET suj_intitule ='".$int."' WHERE suj_id = " . $_POST['sujet'] . ";";
								if (!$result2 = $mysqli->query($sql2)) {
									echo "Error: La requête a echoué \n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {
									header('Location: membre_sujets.php?msg=2');
									exit();
								}

							} else {
								header('Location: membre_sujets.php?msg=4');
								exit();
							}
							

						} else if (isset($_POST['supprimer'])) {

							//Suppression d'un sujet dont l'identifiant est connu de la table de jointure.
							$sql3="DELETE FROM tj_liste WHERE tag_id in (SELECT tag_id FROM t_tag WHERE suj_id = " .$_POST['sujet']. ");";
							if (!$result3 = $mysqli->query($sql3)) {
								echo "Error: La requête a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								exit();
							} else {
								//Suppression de tous les tags dont le sujet est à supprimer.
								$sql4="DELETE FROM t_tag WHERE suj_id = " .$_POST['sujet']. ";";
								if (!$result4 = $mysqli->query($sql4)) {
									echo "Error: La requête a echoué \n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {

									//Suppression d'un sujet dont l'identifiant est connu de la table des tags.
									$sql5="DELETE FROM t_sujet WHERE suj_id = " .$_POST['sujet']. ";";
									if (!$result5 = $mysqli->query($sql5)) {
										echo "Error: La requête a echoué \n";
										echo "Errno: " . $mysqli->errno . "\n";
										echo "Error: " . $mysqli->error . "\n";
										exit();
									} else {
										header('Location: membre_sujets.php?msg=3');
										exit();
									}
								}
							}
						}

						$mysqli->close();

					?>

				</div>
			</div>
		</div>
	</div>
</body>
</html>