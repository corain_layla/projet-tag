<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: deconnection.php
Fonction: Déconnecte un utilisateur connecté et le redirige vers la page d'accueil. 
=========== -->

<?php

session_start();

if(isset($_SESSION['login'])) {
	unset($_SESSION['login']);
	unset($_SESSION['statut']);
	session_destroy();
}

header("Location:index.php");
exit();

?>