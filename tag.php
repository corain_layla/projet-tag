<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: tag.php
Fonction: Page type d'affichage d'un tag dont l'ID est renseignée dans l'URL. Affichage du titre, image, description, et hyperliens. Si l'id n'est pas renseignée ou si le tag est désactivé, un messge d'erreur approprié est affiché. 
=========== -->


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width" />
	<title>Tag</title>
	<link rel="stylesheet" href="css/tag.css" type="text/css">
</head>
<body>

	<div id='contenu'>

	<?php

		if(!isset($_GET['tag'])) {

			echo("</br></br>Erreur, veuillez renseigner un numéro de tag.</br>");
			echo"<a href=\"gestion_tags.php\">Voir tous les tags</a>";
			exit();

		} else {
			
			$num=$_GET['tag'];

			$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

			if ($mysqli->connect_errno) {
			// Affichage d'un message d'erreur
				echo "Error: Problème de connexion à la BDD \n";
				echo "Errno: " . $mysqli->connect_errno . "\n";
				echo "Error: " . $mysqli->connect_error . "\n";
				exit();
			}

			if (!$mysqli->set_charset("utf8")) {
				printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
				exit();
			}

			//sélection de TOUTES les informations d'un tag.
			$sql="SELECT * FROM t_tag LEFT OUTER JOIN tj_liste using (tag_id) LEFT OUTER JOIN t_hyperlien using (lien_id) WHERE tag_id = ".$num.";";
			if (!$result = $mysqli->query($sql)) { 
				// La requête a echoué
				echo "Error: La requête a echoué \n";
				echo "Errno: " . $mysqli->errno . "\n";
				echo "Error: " . $mysqli->error . "\n";
				exit();
			} else {
				$tag = $result->fetch_object();
				if($tag->tag_etat == 'V') {
					echo ("<h1>". $tag->tag_label . "</h1></br></br>");
					echo('<img src="' . $tag->tag_img . '">');
					echo("</br></br><div id='descr'><p>". $tag->tag_contenu . "</br></br>");
					if ($tag->lien_url) {
						echo ("(" . $result->num_rows. ") lien(s) disponibles : </br>");
						echo "<br />";
						echo ('<a href="'. $tag->lien_url . '">' . $tag->lien_nom . '</a><br />');
						while ($lien = $result->fetch_assoc()) {
							echo ('<a href="'. $lien['lien_url'] . '">' . $lien['lien_nom'] . '</a>');
							echo "<br />";
						}
					}
					echo "</p></div>";
				} else {
					echo "</br></br>Nous sommes désolés, ce tag est inactif pour le moment. Veuillez réessayer plus tard.</br>";
					echo"<a href=\"gestion_tags.php\">Voir tous les tags</a>";
					exit();
				}
			}
			
			$mysqli->close();
		}


	?>

	</div>

</body>
</html>