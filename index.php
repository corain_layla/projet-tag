﻿<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: index.php
Fonction: Page d'accueil du site. Affichage de toutes les actualités dans l'ordre chronologique décroissant.
=========== -->

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Aviron Brestois</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li class="selected">
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="compte.php">Compte</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
                <div>
                    <h2>Bienvenue à l'aviron Brestois</h2>
					<p>

                    <?php
                    
						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						echo ("Connexion BDD réussie !");
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}
					?>

                        Vous êtes sur le site de gestion des actualités et des QR codes de l'aviron Brestois. Créez un compte pour commencer ou connectez vous.
                    </p>
                    <img src="images/indeximg.jpg" alt="" class="center">
                    <div>
                        <h3><span>Actualités</span></h3>
                        <p>
                        <?php

                        	//Affichage de toutes les actualités valides de la plus récente à la plus ancienne.
							$sql1="SELECT * FROM t_actualite WHERE actu_etat = 'V' ORDER BY (date_publi) desc;";
							if (!$result1 = $mysqli->query($sql1)) {
								echo "Error: La requête a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								exit();
							} else {
								echo "Total: ";
								echo($result1->num_rows);
								echo "<hr>";
								while ($actu = $result1->fetch_assoc()) {
									echo ("<p>". $actu['actu_titre']);
									echo "<br /><br />";
									echo ('"'. $actu['actu_txt'] . '"');
									echo "<br /><br />";
									echo ('Auteur: ' . $actu['compte_pseudo'] . ', le ' . $actu['date_publi']. "</p><hr>");
									
								}
							}

							$mysqli->close();
						?>

                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
