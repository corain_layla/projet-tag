<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: gestion_actus.php
Fonction: Affichage, création, suppression, modification des tags personnels en fonction de l'utilisateur connecté. Affichage de tous les tags et possibilité de recherche par sujet/auteur.
=========== -->


<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location:index.php");
	exit();
}
?>
<!DOCTYPE html>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Tous les tags</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="compte.php">Compte</a>
					</li>

					<?php
						if ($_SESSION['statut'] == 'M') {
							echo"<li>";
								echo"<a href=\"membre_actus.php\">Mes Actus</a>";
							echo"</li>";
							echo"<li>";
								echo"<a href=\"membre_sujets.php\">Sujets</a>";
							echo"</li>";
						} else if ($_SESSION['statut'] == 'G') {
							echo"<li>";
								echo"<a href=\"gestionnaire_utilisateurs.php\">Utilisateurs</a>";
							echo"</li>";
						}

					?>
					<li class="selected">
						<a href="gestion_tags.php">Tags</a>
					</li>
					<li>
						<a href="deconnection.php">Déconnection</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="portfolio">
					<h2>Gestion des tags</h2>
					
					<?php

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');
						
						if ($mysqli->connect_errno) {
						// Affichage d'un message d'erreur
							echo "Error: Problème de connexion à la BDD \n";
							echo "Errno: " . $mysqli->connect_errno . "\n";
							echo "Error: " . $mysqli->connect_error . "\n";
							// Arrêt du chargement de la page
							exit();
						}
						//echo ("Connexion BDD réussie !");
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							exit();
						}


						if ((!isset($_POST['ajouter'])) && (!isset($_POST['supprimer'])) && (!isset($_POST['modifier'])) )  {


							if ($_SESSION['statut'] == 'M') {

								//sélection des sujets créés par l'utilisateur connecté.
								$sql1 = "SELECT * FROM t_sujet WHERE compte_pseudo = '" .$_SESSION['login']. "';";
								if (!$result1 = $mysqli->query($sql1)) { 
									// La requête a echoué
									echo "Error: La requête a echoué \n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {
									echo "</br></br></br></br><div class=\"about\">";
									echo ('<h4><span>Créer un tag</span></h4>');
									echo("<form action=\"gestion_tags.php\" method=\"post\" enctype=\"multipart/form-data\">");

									echo "<p>Sujet : <select name=\"sujet\" requiered=\"requiered\">";
									while ($suj = $result1->fetch_assoc()) {
										echo ("<option value=\"".$suj['suj_id']."\">" . $suj['suj_intitule'] . "</option>");
									}

									echo"</select></br></br>";

									echo("Titre : <input name=\"titre\" type=\"text\"placeholder=\"...\" maxlength=\"32\" required=\"required\"/></br></br>");
									echo("Texte Contenu : <textarea name=\"contenu\" type=\"text\" required=\"required\" cols=\"89\" rows=\"2\"/></textarea></br></br>");
									echo("Image : <input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\"></br>");
									echo("Message au correcteur: Fonction indisponible sur obiwan2 (??) La création de tag est toujours possible.</br></br>");
									echo("<input name=\"ajouter\" value=\"Ajouter\" type=\"submit\"/></form></p>");

									if (isset($_GET['msg'])) {
										echo"<p>";
										if ($_GET['msg'] == 4) {
											echo("Problème avec l'upload du fichier image.");
										} else if ($_GET['msg'] == 5) {
											echo("Tag créé.");
										} else if ($_GET['msg'] == 6) {
											echo("Aucun sujet sélectionné.");
										}
										echo"</p>";
									}

									//sélection des tags dont les sujets ont étés créés par l'utilisateur connecté.
									$sql2 = "SELECT * FROM t_tag JOIN t_sujet USING (suj_id) WHERE compte_pseudo = '" .$_SESSION['login']. "';";
									if (!$result2 = $mysqli->query($sql2)) {
										echo "Error: La requête a echoué \n";
										echo "Errno: " . $mysqli->errno . "\n";
										echo "Error: " . $mysqli->error . "\n";
									} else {

										echo "</br></br><h4><span>Supprimer un tag</span></h4>";
										echo("<form action=\"gestion_tags.php\" method=\"post\">");
										echo "<p><select name=\"idSuppr\">";
										while ($tag = $result2->fetch_assoc()) { 
											echo ("<option value=\"".$tag['tag_id']."\">" . $tag['tag_id'] ." - ". $tag['tag_label'] . "</option>");
										}
										echo"</select>";
										echo("  <input name=\"supprimer\" value=\"Supprimer\" type=\"submit\"/></form></p>");

										if (isset($_GET['msg'])) {
											if ($_GET['msg'] == 3) {
												echo("<p>Tag supprimé.</p>");
											} else if ($_GET['msg'] == 7) {
												echo("<p>Aucun tag sélectionné.</p>");
											}
										}
									}
								}

							} else if ($_SESSION['statut'] == 'G') {

								//sélection de tous les tags
								$sql1 = "SELECT * FROM t_tag ;";
								if (!$result1 = $mysqli->query($sql1)) {
									echo "Error: La requête a echoué \n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {
									echo "</br></br></br></br><div class=\"about\">";
									echo ('<h4><span>Activer/Désactiver un tag</span></h4>');
									echo("<form action=\"gestion_tags.php\" method=\"post\">");
									echo "<p>Label : <select name=\"id\">";
									while ($tag = $result1->fetch_assoc()) {
										echo ("<option value=\"".$tag['tag_id']."\">" . $tag['tag_label'] . "</option>");
									}
									echo"</select>";

									echo " <select name=\"etat\">";
									echo "<option value=\"V\">Activer</option>";
									echo "<option value=\"I\">Désactiver</option>";
									echo"</select>";

									echo(" <input name=\"modifier\" value=\"Valider\" type=\"submit\"/></form></p>");

								}
							}
							

							echo ('</br><h4><span>Tous les tags</span></h4>');

							if ( (isset($_POST['rechercher'])) && ($_POST['id'] != 0) ) {
								//sélection d'un tag dont l'identifiant est connu ainsi que TOUTES ses informations
								$sql="SELECT DISTINCT * FROM t_sujet LEFT OUTER JOIN t_tag USING (suj_id) LEFT OUTER JOIN tj_liste USING (tag_id) LEFT OUTER JOIN t_hyperlien USING (lien_id) WHERE suj_id =" . $_POST['id'] . " GROUP BY tag_id;";
							} else {
								//sélection de tous les tags ainsi que TOUTES leurs informations
								$sql="SELECT DISTINCT * FROM t_sujet LEFT OUTER JOIN t_tag USING (suj_id) LEFT OUTER JOIN tj_liste USING (tag_id) LEFT OUTER JOIN t_hyperlien USING (lien_id) GROUP BY tag_id;";
							}

							if (!$result = $mysqli->query($sql)) {
								echo "Error: Problème d'accès à la base \n";
								exit();
							} else {
								echo("<form action=\"gestion_tags.php\" method=\"post\">");
								echo "<p> Par sujet ou auteur : <select name=\"id\" requiered=\"requiered\">";
								echo ("<option value=\"0\">Tous</option>");

								//sélection de tous les sujets et leur auteur.
								$sql0 = "SELECT * FROM t_sujet";
								if (!$result0 = $mysqli->query($sql0)) {
									echo "Error: Problème d'accès à la base \n";
									exit();
								} else {
									while ($suj = $result0->fetch_assoc()) { 
										echo ("<option value=\"".$suj['suj_id']."\">" . $suj['suj_intitule'] ." (". $suj['compte_pseudo'] . ")</option>");
									}
								}

								echo"</select>";
								echo("  <input name=\"rechercher\" value=\"Rechercher\" type=\"submit\"/></form></p></div>");

								echo ("<ul>");
								while ($tag = $result->fetch_assoc()) {
									if ($tag['tag_id']) {
										echo("<li>");
										echo("<span>" . $tag['tag_label'] . "</br>Sujet: ". $tag['suj_intitule'] . "</br>Auteur sujet: ". $tag['compte_pseudo'] . "<span> <a href='tag.php?tag=".$tag['tag_id']."'><img src='".$tag['tag_qr']."' alt='' class=\"tag\"></a>");
										echo("</li>");
									}
								}
								echo("</ul>");

							}


						} else if (isset($_POST['supprimer'])) {

							if (isset($_POST['idSuppr'])) {
								$id=htmlspecialchars(addslashes($_POST['idSuppr']));
							} else {
								header("Location: gestion_tags.php?msg=7");
								exit();
							}

							//suppression d'un tag dont l'identifiant est connu dans la table de jointure.
							$sql1 = "DELETE FROM tj_liste WHERE tag_id =". $id . ";";
							if (!$result1 = $mysqli->query($sql1)) { 
								echo "Error: La requête DELETE tj_liste a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								echo $id;
								exit();
							} else {
								//suppression d'un tag dont l'identifiant est connu dans la table des tags.
								$sql2 = "DELETE FROM t_tag WHERE tag_id =". $id . ";";
								if (!$result2 = $mysqli->query($sql2)) {
									echo "Error: La requête DELETE t_tag a echoué \n";
									echo "Errno: " . $mysqli->errno . "\n";
									echo "Error: " . $mysqli->error . "\n";
									exit();
								} else {
									header("Location: gestion_tags.php?msg=3");
									exit();
								}
							}


						} else if (isset($_POST['ajouter'])) {

							if (isset($_FILES['ajouter'])) {

								//ne fonctionne pas quand hébergé sur obiwan2.
								var_dump(basename($_FILES['fileToUpload']['name']));
								$target_dir = __DIR__ . "/tag_images/";
								$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
								$uploadOk = 1;
								$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
								$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
								if($check !== false) {
								    $uploadOk = 1;
								} else {
								    $uploadOk = 0;
								}
								if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
								    $uploadOk = 0;
								}
								if ($uploadOk == 0) {
								    header("Location: gestion_tags.php?msg=4");
									exit();
								} else {
								    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
								        $img=htmlspecialchars(addslashes($target_file));
								    } else {
								        header("Location: gestion_tags.php?msg=4");
										exit();
								    }
								}

							} else {
								$img="tag_images/noIMG.png";
							}


							$suj=htmlspecialchars(addslashes($_POST['sujet']));
							$lbl=htmlspecialchars(addslashes($_POST['titre']));
							$cont=htmlspecialchars(addslashes($_POST['contenu']));
							
							//insertion d'un nouveau tag.
							$sql0 = "INSERT INTO t_tag VALUES (" . $suj . ", null, '" . $lbl . "', '" . $cont . "','".$img."', 'V', 'tag_images/noQR.jpg'); ";
							if (!$result0 = $mysqli->query($sql0)) { 
								// La requête a echoué
								echo "Error: La requête INSERT a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								exit();
							} else {
								header("Location: gestion_tags.php?msg=5");
								exit();
								
							}

						} else if (isset($_POST['modifier'])) {


							//modification de l'état d'un tag dont on a renseigné l'identifiant.
							$sql2 = "UPDATE t_tag SET tag_etat = '" .$_POST['etat']. "' WHERE tag_id = " .$_POST['id']. ";";
							if (!$result2 = $mysqli->query($sql2)) { 
								echo "Error: La requête UPDATE t_tag a echoué \n";
								echo "Errno: " . $mysqli->errno . "\n";
								echo "Error: " . $mysqli->error . "\n";
								exit();
							} else {
								header("Location: gestion_tags.php");
								exit();
								
							}
						}

						$mysqli->close();
					?>	

				</div>
			</div>
		</div>
	</div>
</body>
</html>