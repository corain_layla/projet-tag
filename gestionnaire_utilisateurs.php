<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: gestionnaire_utilisateurs.php
Fonction: Accessible uniquement par un gestionnaire connecté. Affichage et gestion de la validité de tous les utilisateurs enregistrés dans la base de donnée.
=========== -->

<?php
session_start();
if((!isset($_SESSION['login'])) || (($_SESSION['statut']) != 'G')) {
	header("Location:index.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Espace Membre</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="compte.php">Compte</a>
					</li>
					<li class="selected">
						<a href="gestionnaire_utilisateurs.php">Utilisateurs</a>
					</li>
					<li>
						<a href="gestion_tags.php">Tags</a>
					</li>
					<li>
						<a href="deconnection.php">Déconnection</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="about">
					<h2>Gerer les utilisateurs</h2>
					<h4><span>Tous les comptes</span></h4>

					<?php

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}

						if ( (!isset($_POST['pseudo'])) && (!isset($_POST['valide'])) ) {


							//Affichage de tous les comptes, ainsi que leur statut et validité.
							$sql1 = "SELECT * FROM t_compte JOIN t_profil USING (compte_pseudo);";
							if (!$result1 = $mysqli->query($sql1)) {
								echo "Error: La requête select t_compte a échoué \n";
								exit();
							} else {
								$total = $result1->num_rows;
								echo("<p>Nombre total d'utilisateurs : " . $total . "</br></br>");
								while ($utilisateur = $result1->fetch_assoc()) {
									echo ("(" . $utilisateur['pro_valide'] . ") (". $utilisateur['pro_statut'] .") ". $utilisateur['compte_pseudo'] . "</br>");
								}
								$result1->free();
								echo("</p>");
							}

							echo("<h4><span>Activer/Désactiver un profil</span></h4>");
						

							echo "<div id=\"contenu\">";

							echo "<form action=\"gestionnaire_utilisateurs.php\" method=\"post\">";
							
							echo "<p>Utilisateur : <input type=\"text\" name=\"pseudo\" maxlength=\"16\" required=\"required\" placeholder=\"...\"/>";
							echo " Validité du profil : <select name=\"valide\"><option value=\"A\">Actif</option><option value=\"D\">Inactif</option></select>";
							echo"</p>";
							echo"<input type=\"submit\" value=\"Modifier\"></form>";

							if ((isset($_GET['error'])) && ($_GET['error'] == 1)) {
								echo("<p>Utilisateur inexistant.</p>");
							}
							echo"</div>";

						} else {


							$id=htmlspecialchars(addslashes($_POST['pseudo']));
							$valide=htmlspecialchars(addslashes($_POST['valide']));

							//Vérification de l'existance du pseudo donné.
							$sql3 = "SELECT * FROM t_compte WHERE compte_pseudo = '" . $id . "';";
							if (!$result3 = $mysqli->query($sql3)) {
								echo "Error: La requête SELECT a échoué. \n";
								exit();
							} else {
								if($result3->num_rows == 0) {
									header("Location:gestionnaire_utilisateurs.php?error=1");
									exit();
								} else {
									//modification de la validité d'un profil dont le pseudo est connu.
									$sql2 = "UPDATE t_profil SET pro_valide = '" . $valide . "' WHERE compte_pseudo ='" . $id . "';";
									if (!$result2 = $mysqli->query($sql2)) {
										echo "Error: La requête UPDATE a échoué. \n";
										exit();
									} else {
										header("Location:gestionnaire_utilisateurs.php");
										exit();
									}
								}
							}
						}
					
						$mysqli->close();

					?>

				</div>
			</div>
		</div>
	</div>

</body>
</html>