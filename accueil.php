<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: accueil.php
Fonction: Page d'accueil d'un utilisateur, membre ou gestionnaire (différence d'affichage en fonction du statut). Affichage de son profil. Possibilité de modification des informations personnelles.
=========== -->


<?php
session_start();
if (!isset($_SESSION['login'])) {
	header("Location:index.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Espace Membre</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li class="selected">
						<a href="compte.php">Compte</a>
					</li>

					<?php
						if ($_SESSION['statut'] == 'M') {
							echo"<li>";
								echo"<a href=\"membre_actus.php\">Mes Actus</a>";
							echo"</li>";
							echo"<li>";
								echo"<a href=\"membre_sujets.php\">Sujets</a>";
							echo"</li>";
						} else if ($_SESSION['statut'] == 'G') {
							echo"<li>";
								echo"<a href=\"gestionnaire_utilisateurs.php\">Utilisateurs</a>";
							echo"</li>";
						}

					?>
					<li>
						<a href="gestion_tags.php">Tags</a>
					</li>
					<li>
						<a href="deconnection.php">Déconnection</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="about">

					<?php

						if ($_SESSION['statut'] == 'M') {
							echo"<h2>Bienvenue sur l'espace Membre</h2>";
							echo"<img src=\"images/membre.jpg\" alt=\"\" class=\"center\">";
							echo ('<h4><span> Bonjour, ' . $_SESSION['login'] . '</span></h4>');
							echo("<p>Vous êtes bien connecté à votre compte membre. Gérez vos tags, actualités, sujets...</br>Votre profil :</br>");
						} else if ($_SESSION['statut'] == 'G') {
							echo"<h2>Bienvenue sur l'espace Gestionnaire</h2>";
							echo"<img src=\"images/profilimg.jpg\" alt=\"\" class=\"center\">";
							echo ('<h4><span> Bonjour, ' . $_SESSION['login'] . '</span></h4>');
							echo("<p>Vous êtes bien connecté à votre compte membre. Gérez vos tags, actualités, sujets...</br>Votre profil :</br></br>");
						}

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}

						if ( (!isset($_POST['modifier'])) && (!isset($_POST['update'])) ) {

							//Affichage des informations personelles de l'utilisateur dont on connait le pseudo.
							$sql = "SELECT * FROM t_profil JOIN t_compte USING (compte_pseudo) WHERE compte_pseudo = '". $_SESSION['login'] . "';";
							if (!$result = $mysqli->query($sql)) {
								echo "Error: Problème d'accès à la base \n";
								exit();
							} else {
								$infos = $result->fetch_object();
								echo("Nom et prénom: " . $infos->pro_prenom . " " .$infos->pro_nom);
								echo("</br>");
								echo("Adresse Mail: " . $infos->pro_mail);
								echo("</br>");
								echo("Date de création : ". $infos->date_creation);

							}

							echo"<div id='contenu'><form action=\"accueil.php\" method=\"post\">";
							echo"<input type=\"submit\" name =\"modifier\" value=\"Modifier\"></form></p></div>";


						} else if (isset($_POST['modifier'])) {

							echo"<div id='contenu'><p><form action=\"accueil.php\" method=\"post\">";
							echo("<p>Nouveau nom : <input name=\"newnom\" type=\"text\"placeholder=\"...\" maxlength=\"16\" required=\"required\" pattern=\"[A-Za-zÀ-ÿ '-]+\"/></br></br>");
							echo("Nouveau prénom : <input name=\"newprenom\" type=\"text\"placeholder=\"...\" maxlength=\"16\" required=\"required\" pattern=\"[A-Za-zÀ-ÿ '-]+\"/></br></br>");
							echo("Nouveau mail : <input name=\"newmail\" type=\"email\"placeholder=\"...\" maxlength=\"64\" required=\"required\"/></br></br>");
							/*echo"Modifier mot de passe</br></br>";
							echo("Mot de passe actuel : <input name=\"oldmdp\" type=\"password\"placeholder=\"...\" required=\"required\"/> ");
							echo("Nouveau Mot de passe : <input name=\"newmdp\" type=\"password\"placeholder=\"...\" required=\"required\"/></br></br>");*/
							echo"<input type=\"submit\" name =\"update\" value=\"Valider\"></form></p></div>";
							
						} else if (isset($_POST['update'])) {

							$nom=htmlspecialchars(addslashes($_POST['newnom']));
							$prenom=htmlspecialchars(addslashes($_POST['newprenom']));
	                    	$mail=htmlspecialchars(addslashes($_POST['newmail']));

	                    	//modification du profil de l'utilisateur dont on connait le pseudo.
							$sql1="UPDATE t_profil SET pro_nom = '".$nom."', pro_prenom = '".$prenom."', pro_mail = '".$mail."' WHERE compte_pseudo = '" .$_SESSION['login']. "';";
							if (!$result1 = $mysqli->query($sql1)) {
								echo "Error: La requête update a échoué \n";
								 echo "Errno: " . $mysqli->errno . "\n";
								 echo "Error: " . $mysqli->error . "\n";
							} else {
								header("Location: membre_accueil.php");
								exit();
							}

						}

						$mysqli->close();

					?>

				</div>
			</div>
		</div>
	</div>

</body>
</html>