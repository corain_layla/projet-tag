<!-- ===========
Titre: Projet Tag
Auteur: Layla CORAIN L2 info 2
Date: Décembre 2018
Description: gestion des actualités et des tags d’une association sportive (l’aviron brestois). Les tags peuvent être placés sur du matériel (bateaux, équipements de salle, ...), et donnent des informations complémentaires (compétitions, technique, ...)
Version: V2 + quelques fonctionnalités de la V3.
==================
Nom du fichier: membre_actus.php
Fonction: Accessible uniquement par un membre connecté. Affichage, création, modification, suppression des actualités personnels de l'utilisateur connecté.
=========== -->

<?php
session_start();
if((!isset($_SESSION['login'])) || (($_SESSION['statut']) != 'M')) {
	header("Location:index.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Espace Membre</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
	<div class="border">
		<div id="bg">
			background
		</div>
		<div class="page">
			<div class="sidebar">
				<a href="index.php" id="logo"><img src="images/avb.png" alt="logo"></a>
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="compte.php">Compte</a>
					</li>
					<li class="selected">
						<a href="membre_actus.php">Mes Actus</a>
					</li>
					<li>
						<a href="membre_sujets.php">Sujets</a>
					</li>
					<li>
						<a href="gestion_tags.php">Tags</a>
					</li>
					<li>
						<a href="deconnection.php">Deconnection</a>
					</li>
				</ul>
				<form action="about.php">
				</form>
				<div class="connect">
				</div>
				<p>
					Last Update : 
				</p>  
				<p>          
				<script type="text/javascript">
                	d = new Date();
               		document.write(d.toLocaleDateString()+' '+d.toLocaleTimeString());
            	</script>
            	</p>
				<p>
					Site créé par Layla CORAIN © UBO 2018
				</p>
			</div>
			<div class="body">
				<div class="about">
					<h2>Mes actualités</h2>

					<?php

						$mysqli = new mysqli('localhost', 'zcorainla', 'akpbkt8m', 'zfl2-zcorainla');

						if ($mysqli->connect_errno) {
							 // Affichage d'un message d'erreur
							 echo "Error: Problème de connexion à la BDD \n";
							 echo "Errno: " . $mysqli->connect_errno . "\n";
							 echo "Error: " . $mysqli->connect_error . "\n";
							 // Arrêt du chargement de la page
							 exit();
						}
						// Instructions PHP à ajouter pour l'encodage utf8 du jeu de caractères
						if (!$mysqli->set_charset("utf8")) {
							 printf("Pb de chargement du jeu de car. utf8 : %s\n", $mysqli->error);
							 exit();
						}

						if ( (!isset($_POST['supprimer'])) && (!isset($_POST['poster'])) && (!isset($_POST['modifier2']))) {

							if (!isset($_POST['modifier1'])) {

								
								echo "<h4><span>Poster une actualité</span></h4>";							
								echo"<div id=\"contenu\">";
								echo"<form action=\"membre_actus.php\" method=\"post\"><p>";
								echo"Titre : <input name=\"titre\" type=\"text\" maxlength=\"32\" required=\"required\" placeholder=\"...\">";
								echo"</br></br>Texte : </br><textarea name=\"texte\" cols=\"89\" rows=\"3\" required=\"required\"></textarea>";
								echo"</p><input type=\"submit\" name =\"poster\" value=\"Poster\"></form>";
								echo"</div>";

								if (isset($_GET['msg'])) {
									if ($_GET['msg'] == 1) {
										echo("<p>  Actualité mise à jour</p>");
									} else if ($_GET['msg'] == 2) {
										echo("<p>  Actualité supprimée</p>");
									}
								}

							} else {

								echo "<h4><span>Modifier une actualité</span></h4>";							
								echo"<div id=\"contenu\">";
								echo"<form action=\"membre_actus.php\" method=\"post\"><p>";
								echo"<input type=\"hidden\" name =\"idModif2\" value= \"" . $_POST['idModif'] . " \">";
								echo"Nouveau Titre : <input name=\"titreModif\" type=\"text\" maxlength=\"32\" required=\"required\" placeholder=\"...\"></br></br>";
								echo"Nouveau Texte : </br><textarea name=\"texteModif\" cols=\"89\" rows=\"20\" required=\"required\"></textarea>";
								echo"</p><input type=\"submit\" name =\"modifier2\" value=\"Modifier\"></form>";
								echo"</div>";
							}

							

							//Affichage des actualités crées par l'utilisateur connecté.
							$sql1 = "SELECT * FROM t_actualite WHERE compte_pseudo = '". $_SESSION['login'] . "' ORDER BY date_publi desc;";
							if (!$result1 = $mysqli->query($sql1)) {
								echo $sql;
								echo "Error: Problème d'accès à la base \n";
								exit();
							} else {
								echo ("<p>Toutes mes actualités, total: ". $result1->num_rows);
								while ($actu = $result1->fetch_assoc()) {
									echo ("<div id=\"contenu\"><h4><span>Titre : ".$actu['actu_titre']. "</span></h4><p>");
									echo ('"'. $actu['actu_txt'] . '"');
									echo "<br /><br />";
									echo ('Validité: (' . $actu['actu_etat'] . '). Posté le ' . $actu['date_publi']. "</p>");

									echo"<p><form action=\"membre_actus.php\" method=\"post\">";
									echo"<input type=\"hidden\" name =\"idModif\" value= \"" . $actu['actu_id'] . " \">";
									echo"<input type=\"submit\" name = \"modifier1\" value=\"Modifier\"></form>";
									echo("</br>");

									echo"<form action=\"membre_actus.php\" method=\"post\">";
									echo"<input type=\"hidden\" name =\"idSuppr\" value= \"" . $actu['actu_id'] . " \">";
									echo"<input type=\"submit\" name =\"supprimer\" value=\"Supprimer\"></form></p></div></br></br></br>";
								}
							}

						

						} else if (isset($_POST['poster'])) {

							$titre=htmlspecialchars(addslashes($_POST['titre']));
	                    	$txt=htmlspecialchars(addslashes($_POST['texte']));

	                    	//Insertion d'un nouvelle actualité.
							$sql2="INSERT INTO t_actualite VALUES ('" . $_SESSION['login'] . "', null, '" . $titre . "', '" . $txt . "', CURRENT_TIMESTAMP, 'V');";
							if (!$result2 = $mysqli->query($sql2)) {
								 echo "Error: La requête t_actualite a échoué \n";
								 echo "Query: " . $sql2 . "\n";
								 echo "Errno: " . $mysqli->errno . "\n";
								 echo "Error: " . $mysqli->error . "\n";
								 exit();
							} else {
								header("Location:membre_actus.php");
								exit();
							}

						} else if (isset($_POST['modifier2'])) {

							$titre=htmlspecialchars(addslashes($_POST['titreModif']));
	                    	$txt=htmlspecialchars(addslashes($_POST['texteModif']));

	                    	//Modification d'une actualité dont l'identifiant est connu.
							$sql3 = "UPDATE t_actualite SET actu_titre='".$titre."', actu_txt ='".$txt."' WHERE actu_id = ".$_POST['idModif2'].";"; 
							if (!$result3 = $mysqli->query($sql3)) {
								 echo "Error: La requête update a échoué \n";
								 echo "Query: " . $sql3 . "\n";
								 echo "Errno: " . $mysqli->errno . "\n";
								 echo "Error: " . $mysqli->error . "\n";
								 exit();
							} else {
								header("Location:membre_actus.php?msg=1");
								exit();
							}

						} else if (isset($_POST['supprimer'])) {


							//Suppresion d'une actualité dont l'identifiant est connu.
							$sql4 = "DELETE FROM t_actualite WHERE actu_id = ". $_POST['idSuppr'] .";"; 
							if (!$result4 = $mysqli->query($sql4)) {
								 echo "Error: La requête delete a échoué \n";
								 echo "Query: " . $sql4 . "\n";
								 echo "Errno: " . $mysqli->errno . "\n";
								 echo "Error: " . $mysqli->error . "\n";
								 exit();
							} else {
								header("Location:membre_actus.php?msg=2");
								exit();
							}
						
						}

						$mysqli->close();

					?>

				</div>
			</div>
		</div>
	</div>

</body>
</html>